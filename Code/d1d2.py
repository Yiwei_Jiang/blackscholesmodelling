from math import sqrt, log, exp, erf
def d1(S0,K,r,q,sigma,T):
    return (log(S0/K)+(r-q+sigma**2/2)*T)/(sigma*sqrt(T))

def d2(S0,K,r,q,sigma,T):
    return (log(S0/K)+(r-q-sigma**2/2)*T)/(sigma*sqrt(T))