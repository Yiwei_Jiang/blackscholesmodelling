from math import sqrt, log, exp, erf
import random
import numpy as np

import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
import os
import d1d2
S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC
data=pd.read_csv('../Data/BlackScholes.csv',sep=';')


#b-s model
bs_value=[]
for i in range(50,150):
    bs_value.append(S0*stats.norm.cdf(d1d2.d1(S0,strikes[i-50],r,q,vol,T))-strikes[i-50]*exp(-r*T)*stats.norm.cdf(d1d2.d2(S0,strikes[i-50],r,q,vol,T)))
#print(len(bs_value))
analytic_price=pd.DataFrame({'STRIKES':strikes,'OPTIONVAL':bs_value})

#monte carlo
monte_carlo=[]
for i in range(50,150):
    s = np.zeros((Nsteps,1000))
    s[0] = S0
    for j in range(1,Nsteps):
        z = np.random.standard_normal(1000)
        s[j]=s[j-1]*np.exp((r-q-vol**2/2)/Nsteps+vol*sqrt(1/Nsteps)*z)

    c0 = np.exp(-r * T) * np.sum(np.maximum(s[-1]-strikes[i-50], 0)) / 1000
    monte_carlo.append(c0)
#print(monte_carlo)
monte_price=pd.DataFrame({'STRIKES':strikes,'OPTIONVAL':monte_carlo})


plt.plot(data['STRIKES'],data['OPTIONVAL'],'g+')
plt.plot(analytic_price['STRIKES'],analytic_price['OPTIONVAL'],'b-')
plt.plot(monte_price['STRIKES'],monte_price['OPTIONVAL'],'r*')
plt.show()
